# @TEAM-CHIDO

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff?style=for-the-badge)](https://lerna.js.org/)

## Packages

This repository is a monorepo that we manage using Lerna. That means that we actually publish several packages to npm from the same codebase, including:

| Package                  | Version   | Description       |
| ------------------------ | --------- | ----------------- |
| [conventional-commit][1] | ![npm][2] | commitizen module |

[1]: /packages/conventional-commit
[2]: https://img.shields.io/npm/v/@team-chido/conventional-commit?style=for-the-badge
