import wrap from 'word-wrap'
import chalk from 'chalk'

import { Options } from './conventional-commit'

const filter = (array: Array<string>): Array<string> => array.filter(x => x)

const headerLength = (answers: Answers): number =>
  answers.type.length + 2 + (answers.scope ? answers.scope.length + 2 : 0)

const maxSummaryLength = (options: Options, answers: Answers): number =>
  options.maxHeaderWidth - headerLength(answers)

const filterSubject = (subject: string): string => {
  subject = subject.trim()
  if (subject.charAt(0).toLowerCase() !== subject.charAt(0)) {
    subject = subject.charAt(0).toLowerCase() + subject.slice(1, subject.length)
  }
  while (subject.endsWith('.')) {
    subject = subject.slice(0, subject.length - 1)
  }
  return subject
}

export default (options: Options): Record<string, Function> => {
  const types = options.types
  const length =
    Object.keys(types).reduce((acc, cur) =>
      acc.length > cur.length ? acc : cur
    ).length + 1

  const choices = Object.keys(types).map(key => {
    const emoji = options.emojis ? `${types[key].emoji} ` : ''
    return {
      name: `${emoji}${key}:`.padEnd(length) + ' ' + types[key].description,
      value: key
    }
  })

  return {
    prompter: (cz, commit): void => {
      cz.prompt([
        {
          type: 'list',
          name: 'type',
          message: "Select the type of change that you're committing:",
          choices: choices,
          default: options.defaultType
        },
        {
          type: 'input',
          name: 'scope',
          message: 'What is the scope of this change: (press enter to skip)',
          default: options.defaultScope,
          filter: (value: string): string => value.trim().toLowerCase()
        },
        {
          type: 'input',
          name: 'subject',
          message: (answers: Answers): string => {
            return (
              'Write a short, imperative tense description of the change (max ' +
              maxSummaryLength(options, answers) +
              ' chars):\n'
            )
          },
          default: options.defaultSubject,
          validate: (subject: string, answers: Answers): boolean | string => {
            const filteredSubject = filterSubject(subject)
            return filteredSubject.length == 0
              ? 'subject is required'
              : filteredSubject.length <= maxSummaryLength(options, answers)
              ? true
              : 'Subject length must be less than or equal to ' +
                maxSummaryLength(options, answers) +
                ' characters. Current length is ' +
                filteredSubject.length +
                ' characters.'
          },
          transformer: (subject: string, answers: Answers): string => {
            const filteredSubject = filterSubject(subject)
            const color =
              filteredSubject.length <= maxSummaryLength(options, answers)
                ? chalk.green
                : chalk.red
            return color('(' + filteredSubject.length + ') ' + subject)
          },
          filter: (subject: string): string => filterSubject(subject)
        },
        {
          type: 'input',
          name: 'body',
          message:
            'Provide a longer description of the change: (press enter to skip)\n',
          default: options.defaultBody
        },
        {
          type: 'confirm',
          name: 'isBreaking',
          message: 'Are there any BREAKING CHANGE?',
          default: false
        },
        {
          type: 'input',
          name: 'breakingBody',
          default: '-',
          message:
            'A BREAKING CHANGE commit requires a body. Please enter a longer description of the commit itself:\n',
          when: (answers: Answers): boolean =>
            answers.isBreaking && !answers.body,
          validate: (breakingBody: string): boolean | string =>
            breakingBody.trim().length > 0 ||
            'Body is required for BREAKING CHANGE'
        },
        {
          type: 'input',
          name: 'breaking',
          message: 'Describe the breaking changes:\n',
          when: (answers: Answers): boolean => answers.isBreaking
        },

        {
          type: 'confirm',
          name: 'isIssueAffected',
          message: 'Does this change affect any open issues?',
          default: options.defaultIssues ? true : false
        },
        {
          type: 'input',
          name: 'issuesBody',
          default: '-',
          message:
            'If issues are closed, the commit requires a body. Please enter a longer description of the commit itself:\n',
          when: (answers: Answers): boolean | string =>
            answers.isIssueAffected && !answers.body && !answers.breakingBody
        },
        {
          type: 'input',
          name: 'issues',
          message: 'Add issue references (e.g. "fix #123", "re #123".):\n',
          when: (answers: Answers): string => answers.isIssueAffected,
          default: options.defaultIssues ? options.defaultIssues : undefined
        }
      ]).then((answers: Answers) => {
        const wrapOptions = {
          trim: true,
          cut: false,
          newline: '\n',
          indent: '',
          width: options.maxLineWidth
        }

        const scope = answers.scope ? `(${answers.scope})` : ''
        const emoji = options.emojis
          ? options.types[answers.type].emoji + ' '
          : ''

        const head = answers.type + scope + ': ' + emoji + answers.subject
        const body = answers.body ? wrap(answers.body, wrapOptions) : ''

        let breaking = answers.breaking ? answers.breaking.trim() : ''
        breaking = breaking
          ? 'BREAKING CHANGE: ' + breaking.replace(/^BREAKING CHANGE: /, '')
          : ''
        breaking = breaking ? wrap(breaking, wrapOptions) : ''

        const issues = answers.issues ? wrap(answers.issues, wrapOptions) : ''

        commit(filter([head, body, breaking, issues]).join('\n\n'))
      })
    }
  }
}

interface Answers {
  type: string
  scope: string
  isIssueAffected: string
  body: string
  breakingBody: string
  isBreaking: boolean
  breaking: string
  issues: string
  subject: string
}
