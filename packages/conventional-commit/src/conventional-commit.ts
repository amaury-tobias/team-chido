import engine from './engine'
import defaultTypes from './defaults'
import { configLoader } from 'commitizen'

const config: Options = configLoader.load() || {}

const options: Options = {
  emojis: config.emojis || false,
  types: config.types || defaultTypes,
  defaultType: config.defaultType,
  defaultScope: config.defaultScope,
  defaultSubject: config.defaultSubject,
  defaultBody: config.defaultBody,
  defaultIssues: config.defaultIssues,
  maxHeaderWidth: config.maxHeaderWidth || 72,
  maxLineWidth: config.maxLineWidth || 72
}

export default engine(options)

export interface Types {
  description: string
  emoji: string
}

export interface Options {
  emojis: boolean
  types: Record<string, Record<string, string>>
  defaultType: string
  defaultScope: string
  defaultSubject: string
  defaultBody: string
  defaultIssues: string
  maxHeaderWidth: number
  maxLineWidth: number
}
