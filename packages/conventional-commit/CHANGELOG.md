# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [2.1.7](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.6...v2.1.7) (2020-05-16)

**Note:** Version bump only for package @team-chido/conventional-commit





## [2.1.6](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.5...v2.1.6) (2020-05-16)

**Note:** Version bump only for package @team-chido/conventional-commit





## [2.1.5](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.4...v2.1.5) (2020-04-27)

**Note:** Version bump only for package @team-chido/conventional-commit





## [2.1.4](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.3...v2.1.4) (2020-02-24)

**Note:** Version bump only for package @team-chido/conventional-commit






## [2.1.3](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.2...v2.1.3) (2020-02-24)


### Bug Fixes

* fix lib on `tsconfig.json` to use `String.prototype.padEnd()` ([17a6fa8](https://bitbucket.org/amaury-tobias/team-chido/commits/17a6fa8e166d4b9e6984eaa6cd284a0999678501))
* remove unecesary `?` from engine ([23fc4d0](https://bitbucket.org/amaury-tobias/team-chido/commits/23fc4d04ced24c5489815a36a3219f8936dabbe4))






## [2.1.2](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.1...v2.1.2) (2020-01-23)

**Note:** Version bump only for package @team-chido/conventional-commit





## [2.1.1](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.1.0...v2.1.1) (2020-01-21)

**Note:** Version bump only for package @team-chido/conventional-commit





# [2.1.0](https://bitbucket.org/amaury-tobias/team-chido/compare/v2.0.0...v2.1.0) (2020-01-20)


### Features

* **config:** 🚀 add default config on package.json ([06a5222](https://bitbucket.org/amaury-tobias/team-chido/commits/06a522237291ee537336403a0212f1cba1f4b4c9))





# [2.0.0](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.6...v2.0.0) (2020-01-20)


### Bug Fixes

* **engine:** 🐛 filter function, value exixt on choices ([bb4d4d1](https://bitbucket.org/amaury-tobias/team-chido/commits/bb4d4d1436ab12d7988b2760081f2bf551ab742f))


### Code Refactoring

* 💡 remove env vars from constructor ([b28c1c3](https://bitbucket.org/amaury-tobias/team-chido/commits/b28c1c3e3f02c69416c7bc1f8ad2345fa5c111f1))


### BREAKING CHANGES

* 🧨dont use env vars any more





## [1.0.6](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.5...v1.0.6) (2020-01-20)


### Bug Fixes

* **engine:** 🐛 emoji assign on type select ([d3245fc](https://bitbucket.org/amaury-tobias/team-chido/commits/d3245fcd49d4eac39068d09f1660c4398802145e))





## [1.0.5](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.4...v1.0.5) (2020-01-20)


### Bug Fixes

* **index:** 🐛 config assign replace `??` > `||` ([5fb1f03](https://bitbucket.org/amaury-tobias/team-chido/commits/5fb1f03ac62f94ee4d3c4002de40401099656156))





## [1.0.4](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.3...v1.0.4) (2020-01-20)

**Note:** Version bump only for package @team-chido/conventional-commit





## [1.0.3](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.2...v1.0.3) (2020-01-20)

**Note:** Version bump only for package @team-chido/conventional-commit





## [1.0.2](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.1...v1.0.2) (2020-01-20)


### Bug Fixes

* 🐛 path files on `conventional-commit` ([0a86f3f](https://bitbucket.org/amaury-tobias/team-chido/commits/0a86f3f09f64aaa31689df56d4f94713405c0e09))





## [1.0.1](https://bitbucket.org/amaury-tobias/team-chido/compare/v1.0.0...v1.0.1) (2020-01-20)


### Bug Fixes

* 🐛 move .versionrc to root path ([0630345](https://bitbucket.org/amaury-tobias/team-chido/commits/06303459b7a1fb105cbe72a9a0284dd6c1fcffe1))





# 1.0.0 (2020-01-20)

**Note:** Version bump only for package conventional-commit
