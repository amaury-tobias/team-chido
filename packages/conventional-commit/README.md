# conventional-commit

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff?style=for-the-badge)](https://lerna.js.org/)
[![npm](https://img.shields.io/npm/v/@team-chido/conventional-commit?style=for-the-badge)][1]
![license](https://img.shields.io/badge/LICENSE-MIT-44cc00?style=for-the-badge)

Part of the [commitizen](https://github.com/commitizen/cz-cli) family. Prompts for [conventional changelog](https://github.com/conventional-changelog/conventional-changelog) standard.

## Configuration

### Global

install

```sh
npm i -g @team-chido/conventional-commit
```

add file `~/.czrc`

```json
{
  "path": "@team-chido/conventional-commit"
  // ...other configs
}
```

### package.json

Like commitizen, you specify the configuration through the package.json's `config.commitizen` key.

First install

```sh
npm i -D @team-chido/conventional-commit
```

```json
{
// ...  default values
    "config": {
        "commitizen": {
            "path": "./node_modules/@team-chido/conventional-commit",
            "emojis": false,
            "defaultType": "",
            "defaultScope": "",
            "defaultSubject": "",
            "defaultBody": "",
            "defaultIssues": "",
            "maxHeaderWidth": 72,
            "maxLineWidth": 72,
            "types": {
              ...
              "feat": {
                "description": "A new feature",
                "emoji": "🚀"
              },
              ...
            }
        }
    }
// ...
}
```

[1]: https://www.npmjs.com/package/@team-chido/conventional-commit
